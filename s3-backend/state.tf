
resource "aws_s3_bucket" "lab" {
  bucket = "lab-bucket-xyz"

  tags = {
    Name        = "My lab bucket"
    Environment = "Lab"
  }
}

resource "aws_s3_bucket_acl" "lab-s3-acl" {
  bucket = aws_s3_bucket.lab.id
  acl    = "private"
}


resource "aws_dynamodb_table" "terraform-state-lock-dynamodb-table" {
  name           = "terraform-state-lock"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name        = "dynamodb-terraform-lockstate"
    Environment = "lab"
  }
}