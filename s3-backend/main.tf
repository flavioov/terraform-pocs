terraform {
  backend "s3" {
    bucket         = "lab-bucket-xyz"
    key            = "s3/bucket/lab.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.12.1"
    }
  }
}

provider "aws" {
}