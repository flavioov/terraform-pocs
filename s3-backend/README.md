## S3 backend

Save your infrastructure state on a s3 bucket and state lock managed by a dynamodb table 

- aws
- backend (s3)
- s3 (tfstate)
- DynamoDB (lock state)


## kubernetes
- helm
- secret (state)
- provider (kubernetes)

## postgres
- helm (postgres)
- provider (kubernetes)

## etcd3

- helm (etcdv3)
- provider (kubernetes)