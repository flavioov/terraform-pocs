# GitLab ci simple TF Pipeline

OBS: This pipeline considers a remote backend state.

## Stages

- validate
- plan
- apply
- state list
- state pull

### Validate

A simple terraform validation

### Plan

A terraform plan, plus artifact and cache saving for posterior use.

### Apply

Terraform apply with plan as cache

### State

A simple list of objects provisioned and defined at terraform state file. Also a pull of the state and saved as an artifact for download.